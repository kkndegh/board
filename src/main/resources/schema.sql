-- DROP TABLE IF EXISTS member;
-- CREATE TABLE member (
--   member_seq bigint(20) NOT NULL AUTO_INCREMENT COMMENT '회원 식별자',
--   member_id varchar(50) NOT NULL COMMENT '회원 아이디',
--   member_name VARCHAR(20)  COMMENT '회원 이름',
--   member_phone VARCHAR(15)  COMMENT '회원 핸드폰번호',
--   member_del_yn VARCHAR(1)  COMMENT '회원 삭제여부',
--   member_created_date TIMESTAMP  COMMENT '등록일',
--   member_modified_date TIMESTAMP  COMMENT '수정일',
--   PRIMARY KEY (`member_seq`)
-- ) COMMENT='회원정보';

-- INSERT INTO member
-- ( member_id, member_name, member_phone, member_del_yn, member_created_date )
-- VALUES
-- ( 'HONG1', '홍길동', '010-2632-5301', 'N', CURRENT_TIMESTAMP() ),
-- ( 'HONG2', '홍길동2', '010-1234-5302', 'N', CURRENT_TIMESTAMP() ),
-- ( 'HONG3', '홍길동3', '010-2345-5303', 'N', CURRENT_TIMESTAMP() ),
-- ( 'HONG4', '홍길동4', '010-5432-5304', 'N', CURRENT_TIMESTAMP() ),
-- ( 'HONG5', '홍길동5', '010-1523-5305', 'N', CURRENT_TIMESTAMP() ),
-- ( 'HONG6', '홍길동6', '010-6333-5306', 'N', CURRENT_TIMESTAMP() );

DROP TABLE IF EXISTS board;
create table board (
    id bigint not null auto_increment comment 'PK',
    title varchar(100) not null comment '제목',
    content text not null comment '내용',
    writer varchar(20) not null comment '작성자',
    hits int not null comment '조회 수',
    delete_yn enum('Y', 'N') not null comment '삭제 여부',
    created_date datetime not null comment '생성일',
    modified_date datetime comment '수정일',
    primary key (id)
) comment '게시판';